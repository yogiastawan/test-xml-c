/**
 * @file main.c
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>

#include "xdoc/node.h"
#include "xdoc/attribute.h"
#include "xdoc/innertext.h"

#include <gtk/gtk.h>

int main()
{
    puts("hello world!!");
    InnerText *intxt=innerTextNew("first value assigment");
    printf("first: %s\n",getInnerTextContent(intxt));
    setInnerTextContent(intxt,"value changed with this");
    printf("second: %s\n",getInnerTextContent(intxt));
    Node *tag = nodeNew("document", "doc0");
    Node *head = nodeNew("header", "h0");
    Node *body = nodeNew("body", "body");
    addChildNode(tag, head);
    addChildNode(tag, head);
    addChildNode(head,body);
    Attribute *attribute=attributeNew("stroke","solid 1px");
    setNodeAttribute(body,attribute);
    printf("content: %s\n", getTagContent(tag));
    FILE *file = fopen("../test.xml", "w");
    if (file != NULL)
    {
        fprintf(file, getTagContent(tag));
        fclose(file);
    }

    return 0;
}
