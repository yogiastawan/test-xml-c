#include "page.h"

/* Properties enum*/
enum
{
    P_0,    // for padding
    P_VALUE // for value
};

/* Private data structure */
struct _GisaCompassPrivate
{
    // gdouble value;
    GdkWindow *window;
};

GtkWidget *page_new(void)
{
    return (g_object_new(TYPE_PAGE, NULL));
}