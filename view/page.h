/**
 * @file page.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-23
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef _GAKI_PAGE_H_
#define _GAKI_PAGE_H_

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TYPE_PAGE (page_get_type())
#define PAGE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_PAGE, Page))
#define PAGE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), TYPE_PAGE, PageClass))
#define IS_PAGE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_PAGE))
#define IS_COMPASS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TYPE_PAGE))
#define PAGE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_PAGE, PageClass))

typedef struct _page Page;
typedef struct _pageClass PageClass;
typedef struct _pagePrivate PagePrivate;

struct _page
{
    GtkWidget *parent;
    /*<Private>*/
    PagePrivate *priv;
};

struct _pageClass
{
    GtkWidgetClass parent_class;
};
/*Public API */
GType page_get_type(void) G_GNUC_CONST;
GtkWidget *page_new(void);

G_END_DECLS

#endif //_GAKI_PAGE_H_