#include "attribute.h"
#include <stdlib.h>
#include <string.h>

Attribute *attributeNew(char *name, char *value)
{
    Attribute *attribute = (Attribute *)malloc(sizeof(Attribute));
    attribute->name = name;
    attribute->value = value;
    return attribute;
}

char *getAttributContent(Attribute *attribute)
{
    char *content = (char *)malloc(sizeof(char) * 2);
    memcpy(content, " ", 2);
    content = realloc(content, sizeof(char) * (strlen(content) + strlen(attribute->name) + 1));
    memcpy(content + strlen(content), attribute->name, strlen(attribute->name) + 1);
    content = realloc(content, sizeof(char) * (strlen(content) + 3));
    memcpy(content + strlen(content), "=\"", 3);
    content = realloc(content, sizeof(char) * (strlen(attribute->value) + strlen(content) + 1));
    memcpy(content + strlen(content), attribute->value, strlen(attribute->value) + 1);
    content = realloc(content, sizeof(char) * (strlen(content) + 2));
    memcpy(content + strlen(content), "\"", 2);
    return content;
}