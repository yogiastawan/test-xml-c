/**
 * @file attribute.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _GAKI_XML_ATTRIBUTE_H_
#define _GAKI_XML_ATTRIBUTE_H_
typedef struct gaki_attribute Attribute; ///< Attibute object from struct attribute

/**
 * @brief struct of Attribute
 * 
 */
struct gaki_attribute
{
    char *name;  ///< variable to store attibute name
    char *value; ///< Variable to store attribute value
    int index;   ///< Value to store index of this Attribute on Node
};

/**
 * @brief Create new tag Attribute
 * 
 * @param name 
 * @param value 
 * @return Attribute* 
 */
Attribute *attributeNew(char *name, char *value);

/**
 * @brief Get the Attribut object
 * 
 * @param attribute Attribute to get content
 * @return char* 
 */
char *getAttributContent(Attribute *attribute);
#endif //_GAKI_XML_ATTRIBUTE_H_
