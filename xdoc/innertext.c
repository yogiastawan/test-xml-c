#include "innertext.h"
#include <stdlib.h>

InnerText *innerTextNew(char *text)
{
    InnerText *innertext = (InnerText *)malloc(sizeof(InnerText));
    innertext->text = text;
    innertext->index = 0;
}

char *getInnerTextContent(InnerText *innertext)
{
    return innertext->text;
}

void setInnerTextContent(InnerText *innertext, char *text)
{
    innertext->text = text;
}