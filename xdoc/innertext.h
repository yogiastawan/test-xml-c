/**
 * @file innertext.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1
 * @date 2021-10-20
 *
 * @copyright Copyright (c) 2021
 *
 */

typedef struct innertext InnerText;

struct innertext
{
   char *text;
   long index;
};

/**
 * @brief Create new inner text;
 *
 * @param text ///< Text content off inner text
 * @return InnerText* ///< Return innertezt pointer
 */
InnerText *innerTextNew(char *text);

/**
 * @brief Get the Inner Text Content object
 *
 * @param innertext ///< Get content of this innnertext
 * @return char* ///< return text of innertext
 */
char *getInnerTextContent(InnerText *innertext);

void setInnerTextContent(InnerText *innertext, char *text);