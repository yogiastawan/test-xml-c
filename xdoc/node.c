#include "node.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

Node *nodeNew(char *tag, char *id)
{
    Node *node = (Node *)malloc(sizeof(Node));
    node->tag = tag;
    node->id = id;
    node->numbChild = 0;
    node->childNodes = NULL;
    node->numbAttribute = 0;
    node->attributes = NULL;
    node->numbNameSpace = 0;
    node->xmlNps = NULL;
    node->pathIndex = NULL;
    node->level = 0;
    return node;
}

char *getTagContent(Node *node)
{
    char *tag = node->tag;
    char *content = (char *)malloc(sizeof(char) * 2);
    memcpy(content, "<", 2);
    content = realloc(content, sizeof(char) * (strlen(content) + strlen(tag) + 1));
    memcpy(content + strlen(content), tag, strlen(tag) + 1);

    //add namespace
    if (node->xmlNps != NULL)
    {
        int i = 0;
        for (i = 0; i < node->numbNameSpace; i++)
        {
            char *nsp = getXMLNameSpaceContent(node->xmlNps[i]);
            content = realloc(content, sizeof(char) * (strlen(content) + strlen(nsp) + 1));
            memcpy(content + strlen(content), nsp, strlen(nsp) + 1);
        }
    }

    //add attribute
    if (node->attributes != NULL)
    {
        int i = 0;
        for (i = 0; i < node->numbAttribute; i++)
        {
            char *attribute = getAttributContent(node->attributes[i]);
            content = realloc(content, sizeof(char) * (strlen(attribute) + strlen(content) + 1));
            memcpy(content + strlen(content), attribute, strlen(attribute) + 1);
        }
    }

    char *end = (char *)malloc(sizeof(char) * 3);
    memcpy(end, "/>", 3);
    if (node->childNodes != NULL)
    {
        content = realloc(content, sizeof(char) * (strlen(content) + 2));
        memcpy(content + strlen(content), ">", 2);
        int i = 0;
        for (i = 0; i < node->numbChild; i++)
        {
            char *childContent = getTagContent(node->childNodes[i]);
            content = realloc(content, sizeof(char) * (1 + strlen(content) + strlen(childContent)));
            memcpy(content + strlen(content), childContent, strlen(childContent) + 1);
            memcpy(end, "</", 3);
            end = realloc(end, sizeof(char) * (strlen(tag) + 2));
            memcpy(end + strlen(end), tag, strlen(tag) + 1);
            memcpy(end + strlen(end), ">", 2);
        }
    }
    content = realloc(content, sizeof(char) * (strlen(content) + strlen(end) + 1));
    memcpy(content + strlen(content), end, strlen(end) + 1);
    free(end);
    return content;
}

void setNodeId(Node *node, char *id)
{
    node->id = id;
}

void addChildNode(Node *node, Node *childNode)
{
    if (node->childNodes == NULL)
    {
        node->childNodes = (Node **)malloc(sizeof(Node));
    }
    else
    {
        node->childNodes = (Node **)realloc(node->childNodes, sizeof(Node) * (node->numbChild + 1));
    }

    //set level
    childNode->level = node->level + 1;
    //set path index
    if (node->pathIndex == NULL)
    {
        childNode->pathIndex = (long *)malloc(sizeof(long));
    }
    else
    {
        childNode->pathIndex = (long *)realloc(childNode->pathIndex, sizeof(long) * childNode->level);
        childNode->pathIndex = node->pathIndex;
    }
    childNode->pathIndex[node->level] = node->numbChild;

    // add child
    node->childNodes[node->numbChild] = childNode;
    node->numbChild++;
}

void setNodeAttribute(Node *node, Attribute *attribute)
{
    if (node->attributes == NULL)
    {
        node->attributes = (Attribute **)malloc(sizeof(Attribute));
    }
    else
    {
        node->attributes = (Attribute **)realloc(node->attributes, sizeof(Attribute) * (node->numbAttribute + 1));
    }

    attribute->index = node->numbAttribute;
    node->attributes[node->numbAttribute] = attribute;
    node->numbAttribute++;
}

void setNodeXmlNameSpace(Node *node, XMLNameSpace *nps)
{
    if (node->xmlNps == NULL)
    {
        node->xmlNps = (XMLNameSpace **)malloc(sizeof(XMLNameSpace));
    }
    else
    {
        node->xmlNps = (XMLNameSpace **)realloc(node->xmlNps, sizeof(XMLNameSpace) * (node->numbNameSpace + 1));
    }
    node->xmlNps[node->numbNameSpace] = nps;
    node->numbNameSpace++;
}