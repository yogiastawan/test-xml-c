/**
 * @file tag.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _GAKI_NODE_H_
#define _GAKI_NODE_H_

#include "attribute.h"
#include "xmlnamespace.h"

typedef struct gaki_node Node; ///< Node object from struct tag

/**
 * @brief Struct for Node
 * 
 */
struct gaki_node
{
    char *id;               ///< Varibale to store id of Node
    long numbChild;         ///< Varibale to store number of ChildTag on this Node
    char *tag;              ///< Variable to store node tag
    Node **childNodes;      ///< Varibale to store ChildTags
    long numbAttribute;     ///< Variable to store number of Attribute on this Node
    Attribute **attributes; ///< Variable to store Attributes
    long numbNameSpace;     ///< Variable to store number of name space
    XMLNameSpace **xmlNps;  ///< to store name space
    long *pathIndex;        ///< index position node, NULL if it's root
    long level;             ///< level of node, 0 if it's root
};

/**
 * @brief Get the Node Content object
 * 
 * @param node The Node pointer to get the content
 * @return char* 
 */
char *getTagContent(Node *node);

/**
 * @brief Create new Node pointer
 * 
 * @param name Name of Node
 * @param id Id of Node
 * @param index Index of Node
 * @return Node* 
 */
Node *nodeNew(char *name, char *id);

/**
 * @brief Set the Node Id object
 * 
 * @param node Node to set the id
 * @param id New id of tag
 */
void setNodeId(Node *node, char *id);

/**
 * @brief Add child to Node
 * 
 * @param node Node to add child
 * @param childTag child to add the Parent Node
 */
void addChildNode(Node *node, Node *childTag);

/**
 * @brief Set the Node Attribute object
 * 
 * @param node 
 * @param attribute 
 */
void setNodeAttribute(Node *node, Attribute *attribute);

/**
 * @brief Set the Node Xml Name Space object
 * 
 * @param node 
 * @param nps 
 */
void setNodeXmlNameSpace(Node *node, XMLNameSpace *nps);

#endif //_GAKI_NODE_H_