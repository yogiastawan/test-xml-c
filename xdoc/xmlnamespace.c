#include "xmlnamespace.h"
#include <stdlib.h>
#include <string.h>

XMLNameSpace *xmlNameSpaceNew(char *prefix, char *uri)
{
    XMLNameSpace *nps = (XMLNameSpace *)malloc(sizeof(XMLNameSpace));
    nps->prefix = prefix;
    nps->uri = uri;
    return nps;
}

char *getXMLNameSpaceContent(XMLNameSpace *nps)
{
    char *ctn = (char *)malloc(sizeof(char) * 8);
    memcpy(ctn, " xmlns:", 8);
    ctn = (char *)realloc(ctn, sizeof(char) * (strlen(ctn) + strlen(nps->prefix) + 1));
    memcpy(ctn + strlen(ctn), nps->prefix, strlen(nps->prefix) + 1);
    ctn = (char *)realloc(ctn, sizeof(char) * (strlen(ctn) + 3));
    memcpy(ctn + strlen(ctn), "=\"", 3);
    ctn = (char *)realloc(ctn, sizeof(char) * (strlen(ctn) + strlen(nps->uri) + 1));
    memcpy(ctn + strlen(ctn), nps->uri, strlen(nps->uri) + 1);
    ctn = (char *)realloc(ctn, sizeof(char) * (strlen(ctn) + 2));
    memcpy(ctn + strlen(ctn), "\"", 2);
    return ctn;
}
