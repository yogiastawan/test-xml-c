/**
 * @file namespace.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _GAKI_XML_NAME_SPACE_
#define _GAKI_XML_NAME_SPACE_

typedef struct gaki_namespace XMLNameSpace;

struct gaki_namespace
{
    char *prefix;
    char *uri;
    int index;
};

/**
 * @brief Create NameSpace
 * 
 * @param prefix 
 * @param uri 
 * @return NameSpace* 
 */
XMLNameSpace *xmlNameSpaceNew(char *prefix, char *uri);

/**
 * @brief Get the XML Name Space Content object
 * 
 * @param nps 
 * @return char* 
 */
char *getXMLNameSpaceContent(XMLNameSpace *nps);

#endif //_GAKI_XML_NAME_SPACE_